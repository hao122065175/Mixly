# 1 "D:\\GitHub\\Mixly_release\\testArduino\\testArduino.ino"

# 3 "D:\\GitHub\\Mixly_release\\testArduino\\testArduino.ino" 2

long ir_item;
IRrecv irrecv_0(0);
decode_results results_0;

void setup(){
  Serial.begin(9600);
  irrecv_0.enableIRIn();
}

void loop(){
  if (irrecv_0.decode(&results_0)) {
    ir_item=results_0.value;
    String type="UNKNOWN";
    String typelist[18]={"UNUSED", "RC5", "RC6", "NEC", "SONY", "PANASONIC", "JVC", "SAMSUNG", "WHYNTER", "AIWA_RC_T501", "LG", "SANYO", "MITSUBISHI", "DISH", "SHARP", "DENON", "PRONTO", "LEGO_PF"};
    if(results_0.decode_type>=1&&results_0.decode_type<=17){
      type=typelist[results_0.decode_type];
    }
    Serial.println("IR TYPE:"+type+"  ");
    Serial.println(ir_item,16);
    irrecv_0.resume();
  } else {
  }

}
